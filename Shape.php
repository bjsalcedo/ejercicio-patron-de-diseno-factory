<?php
namespace FactoryMethod;

interface IShape
{
  public function getType();
  public function getArea();
  public function getHeight();
  public function getDiameter();
  public function getBase();
}

abstract class ShapeAbstract implements IShape
{
  protected $type;
  protected $radius;
  protected $side;
  protected $base;
  protected $height;  

  public function getType() {
    return $this->type; 
  }

  public function getArea() {
    switch($this->type){
      case 'circle':
        return round(pi() * pow($this->radius, 2), 2) . " m squared";
        break;
      case 'square':
        return pow($this->side, 2)  . " m squared";
        break;
      case 'triangle':
        return ($this->base*$this->height) / 2 . " m squared";
        break;  
    }
  }

  public function getHeight() {
    switch($this->type){
      case 'circle':
        return 'null';
        break;
      case 'square':
        return $this->side;
        break;
      case 'triangle':
        return $this->height;
        break;
    }
  }

  public function getDiameter() {
    switch($this->type){
      case 'circle':
        return $this->radius * 2;
        break;
      case 'square':
        return 'null';
        break;
      case 'triangle':
        return 'null';
        break;
    }
  }

  public function getBase() {
    switch($this->type){
      case 'circle':
        return 'null';
        break;
      case 'square':
        return $this->side;
        break;
      case 'triangle':
        return $this->base;
        break;
    }
  }


}
