<?php
namespace FactoryMethod;

abstract class Maker
{
	abstract public function factoryMethod($shape);
}