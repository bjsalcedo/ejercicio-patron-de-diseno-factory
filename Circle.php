<?php
namespace FactoryMethod;

class Circle extends ShapeAbstract
{
  protected $type;
  protected $radius;

  public function __construct($type, $radius) {
    $this->type = $type;
    $this->radius = $radius;
  }

}
