<?php
namespace FactoryMethod;

class Square extends ShapeAbstract
{
  protected $type;
  protected $side;

  public function __construct($type, $side) {
    $this->type = $type;
    $this->side = $side;
  }

}
