<?php
namespace FactoryMethod;
require ('ConcreteMaker.php');  

/**
 * 
 * Params: square, triangle, circle
 */
$shape = ConcreteMaker::factoryMethod('square');

/**
 * 
 * 
 * Get all properties on screen
 */
echo "Type of shape: ".$shape->getType()."\n";
echo "Area of shape: ".$shape->getArea()."\n";
echo "Height of shape: ".$shape->getHeight()."\n"; 
echo "Diameter of shape: ".$shape->getDiameter()."\n";
echo "Base of shape: ".$shape->getBase();