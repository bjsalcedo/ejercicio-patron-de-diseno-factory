<?php
namespace FactoryMethod;
require ('Maker.php');
require ('Shape.php');
require ('Circle.php');
require ('Square.php');
require ('Triangle.php');

/**
 * 
 * Set props of each object
 */

class ConcreteMaker extends Maker
{
  public function factoryMethod($type){
    switch($type){
      case 'circle':
        return new Circle($type, 15);
        break;
      case 'square':
        return new Square($type, 10);
        break;
      case 'triangle':
        return new Triangle($type, 30, 40);
        break;
    }
  }
}


