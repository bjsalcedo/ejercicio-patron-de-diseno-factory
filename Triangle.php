<?php
namespace FactoryMethod;

class Triangle extends ShapeAbstract
{
  protected $type;
  protected $base;
  protected $height;

  public function __construct($type, $base, $height) {
    $this->type = $type;
    $this->base = $base;
    $this->height = $height;
  }

}
